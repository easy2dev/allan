const db = require("../database");


exports.getParticipante = async (req, res, next) => {
    try {
      const rows = await db('participante');
      res.status(200).send({ rows });
    } catch (error) {
      res.status(500).send(error.message);
    }
  }


  exports.getParticipanteId = async (req, res, next) => {
    const id = req.params.id_participante;
    try {
      //const sqlQuery = "SELECT * from participante where id=? ";
   //const rows = await db.raw(sqlQuery, id);
   const rows = await db('participante').where("id",id);
   
   res.status(200).send({ rows });
   } catch (error) {
     res.status(500).send(error.message);
    }
  }

  exports.getParticipanteNome = async (req, res, next) => {
    const nome = req.params.nome_participante;
    try {
      //const sqlQuery = "SELECT * from participante where id=? ";
   //const rows = await db.raw(sqlQuery, id);
   const rows = await db('participante').where("nome" ,'like',`%${nome}%`);
   
   res.status(200).send({ rows });
   } catch (error) {
     res.status(500).send(error.message);
    }
  }

  exports.deleteParticipanteNome = async (req, res, next) => {
    const nome = req.params.nome_participante;
    try {
      //const sqlQuery = "SELECT * from participante where id=? ";
   //const rows = await db.raw(sqlQuery, id);
   const rows = await db('participante').where("nome" ,'like',`%${nome}%`).del();
   
   res.status(200).send({ rows });
   } catch (error) {
     res.status(500).send(error.message);
    }
  }
  
  exports.deleteParticipanteId = async (req, res, next) => {
    const id = req.params.id_participante;
    try {
      //const sqlQuery = "SELECT * from participante where id=? ";
   //const rows = await db.raw(sqlQuery, id);
   const rows = await db('participante').where("id" ,`${id}`).del();
   
   res.status(200).send({ rows });
   } catch (error) {
     res.status(500).send(error.message);
    }
  }